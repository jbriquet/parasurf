/*
 * File:   FastHessian.cpp
 * Author: jules
 *
 * Created on 19 juin 2014, 15:27
 */

#include "FastHessian.h"
#include "InterestPoint.h"

FastHessian::FastHessian(Mat *img, const int octaves, const int intervals, const int step, const float threshold)
: _integralImg(img) {

    int filterMapping[base_octave][base_interval] = {
        {0, 1, 2, 3},
        {1, 3, 4, 5},
        {3, 5, 6, 7},
        {5, 7, 8, 9},
        {7, 9, 10, 11}
    };

    this->_iptVect.clear();

    int z = 0;

    ChangeParameters(octaves, intervals, step, threshold);

    ComputeSpaceScale();

    FilteredLayer *base, *middle, *high;

    for (int i = 0; i < _octavesNbr; ++i) {
        for (int j = 0; j <= 1; ++j) {



            base = _filteredImgMap.at(filterMapping[i][j]);
            middle = _filteredImgMap.at(filterMapping[i][j + 1]);
            high = _filteredImgMap.at(filterMapping[i][j + 2]);

            // Iterating over the higher layer to find maxima through different scale

            //std::cout << "Height" << high->GetHeight() << std::endl;
            for (int y = 0; y < high->GetHeight(); ++y) {
                for (int x = 0; x < high->GetWidth(); ++x) {
                    // Non Maximal Suppression
                    if (isExtremum(x, y, high, middle, base)) {
                        z++;
                        interpolateExtremum(x, y, high, middle, base);
                        //std::cout << "PASSE: " << z << " fois !" << std::endl;
                    }
                }
            }
        }
    }

    //std::cout << "True: " << tnbr << std::endl;
    //std::cout << "False: " << fnbr << std::endl;
    //std::cout << "Test: " << t << std::endl;
}

void FastHessian::SetIptVect(std::vector<InterestPoint> _iptVect) {
    this->_iptVect = _iptVect;
}

std::vector<InterestPoint> &FastHessian::GetIptVect() {
    return _iptVect;
}

IntegralImage* FastHessian::GetIntegralImg() {
    return &_integralImg;
}

FastHessian::~FastHessian() {
}

void FastHessian::ChangeParameters(const int octaves, const int intervals, const int step, const float threshold) {

    // Check to avoid bad values
    if (octaves > 0 && octaves <= 5)
        this->_octavesNbr = octaves;
    else
        this->_octavesNbr = base_octave;

    if (intervals > 0 && intervals <= 4)
        this->_intervalsNbr = intervals;
    else
        this->_intervalsNbr = base_interval;

    if (step > 0 && step <= 6)
        this->_step = step;
    else
        this->_step = base_step;

    if (threshold > 0)
        this->_thresholdValue = threshold;
    else
        this->_thresholdValue = base_threshold;


}

void FastHessian::ComputeSpaceScale() {

    for (int i = 0; i < _filteredImgMap.size(); ++i)
        delete _filteredImgMap[i];

    _filteredImgMap.clear();

    int imgWidth = _integralImg.GetIntegralImg().cols / _step;
    int imgHeight = _integralImg.GetIntegralImg().rows / _step;

    if (_octavesNbr >= 1) {
        _filteredImgMap.push_back(new FilteredLayer(imgWidth, imgHeight, _step, 9));
        _filteredImgMap.push_back(new FilteredLayer(imgWidth, imgHeight, _step, 15));
        _filteredImgMap.push_back(new FilteredLayer(imgWidth, imgHeight, _step, 21));
        _filteredImgMap.push_back(new FilteredLayer(imgWidth, imgHeight, _step, 27));
    }

    if (_octavesNbr >= 2) {
        _filteredImgMap.push_back(new FilteredLayer(imgWidth / 2, imgHeight / 2, _step * 2, 39));
        _filteredImgMap.push_back(new FilteredLayer(imgWidth / 2, imgHeight / 2, _step * 2, 51));
    }

    if (_octavesNbr >= 3) {
        _filteredImgMap.push_back(new FilteredLayer(imgWidth / 4, imgHeight / 4, _step * 4, 75));
        _filteredImgMap.push_back(new FilteredLayer(imgWidth / 4, imgHeight / 4, _step * 4, 99));
    }

    if (_octavesNbr >= 4) {
        _filteredImgMap.push_back(new FilteredLayer(imgWidth / 8, imgHeight / 8, _step * 8, 147));
        _filteredImgMap.push_back(new FilteredLayer(imgWidth / 8, imgHeight / 8, _step * 8, 195));
    }

    if (_octavesNbr >= 5) {
        _filteredImgMap.push_back(new FilteredLayer(imgWidth / 16, imgHeight / 16, _step * 16, 291));
        _filteredImgMap.push_back(new FilteredLayer(imgWidth / 16, imgHeight / 16, _step * 16, 387));
    }

    for (int i = 0; i < _filteredImgMap.size(); ++i)
        ComputeHessian(_filteredImgMap[i]);
}

void FastHessian::ComputeHessian(FilteredLayer* layer) {

    float *hessDet = layer->GetHessiansDet();
    bool *laplacian = layer->getLaplacian();

    int step = layer->GetStep();
    int borderSize = (layer->GetFilterSize() - 1) / 2; // Get the gray border size

    // the Lobe Size is the size of the white/black part of the weighted box
    int lobeSize = layer->GetFilterSize() / 3;

    int filterSize = layer->GetFilterSize();

    float Dxx, Dyy, Dxy;

    int curX = 0;
    int curY = 0;

    float area = filterSize * filterSize;

    int idx = 0;

    for (int i = 0; i < layer->GetHeight(); ++i) {
        for (int j = 0; j < layer->GetWidth(); ++j) {

            // Match filter position with img pos
            curX = j * step;
            curY = i * step;

            Dxy = // Black Part
                    _integralImg.AreaIntegral(curX + 1, curY - lobeSize, lobeSize, lobeSize)
                    + _integralImg.AreaIntegral(curX - lobeSize, curY + 1, lobeSize, lobeSize)
                    // White Part
                    - _integralImg.AreaIntegral(curX - lobeSize, curY - lobeSize, lobeSize, lobeSize)
                    - _integralImg.AreaIntegral(curX + 1, curY + 1, lobeSize, lobeSize);

            /*Dyy = // Black Part
                    _integralImg.AreaIntegral(curX - (((filterSize - 1) / 2) - borderSize), curY - ((lobeSize - 1) / 2), filterSize - (borderSize * 2), lobeSize) * 3
                    // White Part
                    - _integralImg.AreaIntegral(curX - (((filterSize - 1) / 2) - borderSize), curY - (filterSize - 1 / 2), filterSize - (borderSize * 2), lobeSize * 3);

            Dxx = // Black Part
                    _integralImg.AreaIntegral(curX - ((lobeSize - 1) / 2), curY - (((filterSize - 1) / 2) - borderSize), lobeSize, filterSize - (borderSize * 2)) * 3
                    // White Part
                    - _integralImg.AreaIntegral(curX - (filterSize - 1 / 2), curY - (((filterSize - 1) / 2) - borderSize), lobeSize * 3, filterSize - (borderSize * 2));

             */


            Dxx = _integralImg.AreaIntegral(curX - borderSize, curY - lobeSize + 1, filterSize, 2 * lobeSize - 1)
                    - _integralImg.AreaIntegral(curX - lobeSize / 2, curY - lobeSize + 1, lobeSize, 2 * lobeSize - 1)*3;


            Dyy = _integralImg.AreaIntegral(curX - lobeSize + 1, curY - borderSize, 2 * lobeSize - 1, filterSize)
                    - _integralImg.AreaIntegral(curX - lobeSize + 1, curY - lobeSize / 2, 2 * lobeSize - 1, lobeSize)*3;

            Dxx /= area;
            Dyy /= area;
            Dxy /= area;


            hessDet[idx] = (Dxx * Dyy - 0.81f * Dxy * Dxy);

            if (Dxx + Dyy >= 0)
                laplacian[idx] = 1;
            else
                laplacian[idx] = 0;

            if (idx == 200) {
                //std::cout << idx << "." << std::endl;
                //std::cout << "Dxx: " << Dxx << std::endl;
                //std::cout << "Dyy: " << Dyy << std::endl;
                //std::cout << "Dxy: " << Dxy << std::endl;
                //std::cout << "x: " << curX;
                //std::cout << " - y: " << curY;
                //std::cout << " - filSize: " << filterSize;
                //std::cout << " - lobe: " << lobeSize << std::endl;
                //std::cout << "IImage: " << _integralImg.AreaIntegral(curX + 1, curY - lobeSize, lobeSize, lobeSize) << std::endl;
            }

            idx++;
        }
    }

}

bool FastHessian::isExtremum(int x, int y, FilteredLayer *high, FilteredLayer *middle, FilteredLayer *base) {

    // Check we aren't on border
    int borderLayer = (high->GetFilterSize() + 1) / (2 * high->GetStep());
    if (x <= borderLayer || y <= borderLayer || x >= high->GetWidth() - borderLayer || y >= high->GetHeight() - borderLayer)
        return false;

    int Middlescale = middle->GetWidth() / high->GetWidth();
    int BaseScale = base->GetWidth() / high->GetWidth();

    float curPixel = middle->getScaledHessianDet(x, y, Middlescale);

    // Apply thresholding
    if (curPixel < this->_thresholdValue)
        return false;


    for (int cy = y - 1; cy <= y + 1; ++cy) {
        for (int cx = x - 1; cx <= x + 1; ++cx) {

            // Check in high, base and middle (if not current pixel)
            if (high->getHessianDet(cx, cy) >= curPixel || base->getScaledHessianDet(cx, cy, BaseScale) >= curPixel
                    || ((cx != x || cy != y) && middle->getScaledHessianDet(cx, cy, Middlescale) >= curPixel)) {

                /*float hpx = high->getHessianDet(cx, cy);
                float mpx = middle->getScaledHessianDet(cx, cy, Middlescale);
                float bpx = base->getScaledHessianDet(cx, cy, BaseScale);
                std::cout << "x: " << cx << " - y: " << cy << " - CurPix: " << curPixel << " - HPix: " << hpx << " - MPix: " << mpx << " - BPix: " << bpx << std::endl;
                 */
                return false;
            }
        }
    }

    /*for (int rr = -1; rr <= 1; ++rr) {
        for (int cc = -1; cc <= 1; ++cc) {
            // if any response in 3x3x3 is greater candidate not maximum
            if (
                    high->getHessianDet(x + rr, y + cc) >= curPixel ||
                    ((rr != 0 || cc != 0) && middle->getScaledHessianDet(x + rr, y + cc, Middlescale) >= curPixel) ||
                    base->getScaledHessianDet(x + rr, y + cc, BaseScale) >= curPixel
                    )
                return false;
        }
    }*/

    return true;
}

// Get d^2(H)/d(x^2) Matrix => Hessian Matrix for a pixel
// See derivative on EdgeDetection Power Point

Mat FastHessian::getHessianMatrix(int x, int y, FilteredLayer* high, FilteredLayer* middle, FilteredLayer* base) {
    Mat hessMat(3, 3, CV_64FC1);

    int MiddleScale = middle->GetWidth() / high->GetWidth();
    int BaseScale = base->GetWidth() / high->GetWidth();

    // See second derivative function in Image Processing definition

    double fij = middle->getScaledHessianDet(x, y, MiddleScale);

    double dxx = middle->getScaledHessianDet(x + 1, y, MiddleScale) - 2 * fij + middle->getScaledHessianDet(x - 1, y, MiddleScale);
    double dyy = middle->getScaledHessianDet(x, y + 1, MiddleScale) - 2 * fij + middle->getScaledHessianDet(x, y - 1, MiddleScale);
    double dss = high->getHessianDet(x, y) - 2 * fij + base->getScaledHessianDet(x, y, BaseScale);

    double dxy = (middle->getScaledHessianDet(x + 1, y + 1, MiddleScale) - middle->getScaledHessianDet(x - 1, y + 1, MiddleScale) - middle->getScaledHessianDet(x + 1, y - 1, MiddleScale)
            + middle->getScaledHessianDet(x - 1, y - 1, MiddleScale)) / 4.0;

    double dxs = (high->getHessianDet(x + 1, y) - base->getScaledHessianDet(x + 1, y, BaseScale) - high->getHessianDet(x - 1, y)
            + base->getScaledHessianDet(x - 1, y, BaseScale)) / 4.0;

    double dys = (high->getHessianDet(x, y + 1) - base->getScaledHessianDet(x, y + 1, BaseScale) - high->getHessianDet(x, y - 1)
            + base->getScaledHessianDet(x, y - 1, BaseScale)) / 4.0;

    // First row
    hessMat.at<double>(0, 0) = dxx;
    hessMat.at<double>(0, 1) = dxy;
    hessMat.at<double>(0, 2) = dxs;

    // Second row
    hessMat.at<double>(1, 0) = dxy;
    hessMat.at<double>(1, 1) = dyy;
    hessMat.at<double>(1, 2) = dys;

    // Third row
    hessMat.at<double>(2, 0) = dxs;
    hessMat.at<double>(2, 1) = dys;
    hessMat.at<double>(2, 2) = dss;

    if (x == 126 && y == 23 || (x == 136 && y == 28)) {
        //std::cout << "dxy: " << dxy << std::endl;

        double fr = (middle->getScaledHessianDet(x + 1, y + 1, MiddleScale));

        //std::cout << "first: " << fr << std::endl;
        //std::cout << "sec: " << middle->getScaledHessianDet(x - 1, y + 1, scale) << std::endl;
        //std::cout << "thrd: " << middle->getScaledHessianDet(x + 1, y - 1, scale) << std::endl;
        //std::cout << "frth: " << middle->getScaledHessianDet(x - 1, y - 1, scale) << std::endl;

    }

    return hessMat;

}

// Get d(H)/d(x) Matrix => Hessian Vector for a pixel
// See derivative on EdgeDetection Power Point

Mat FastHessian::getHessianVector(int x, int y, FilteredLayer* high, FilteredLayer* middle, FilteredLayer* base) {
    Mat hessVect(3, 1, CV_64FC1);

    int middleScale = middle->GetWidth() / high->GetWidth();
    int baseScale = base->GetWidth() / high->GetWidth();

    double dx = (middle->getScaledHessianDet(x + 1, y, middleScale) - middle->getScaledHessianDet(x - 1, y, middleScale)) / 2.0;
    double dy = (middle->getScaledHessianDet(x, y + 1, middleScale) - middle->getScaledHessianDet(x, y - 1, middleScale)) / 2.0;
    double ds = (high->getHessianDet(x, y) - base ->getScaledHessianDet(x, y, baseScale)) / 2.0;

    hessVect.at<double>(0, 0) = dx;
    hessVect.at<double>(1, 0) = dy;
    hessVect.at<double>(2, 0) = ds;

    if (x == 126 && y == 23) {

        //std::cout << "dx: " << dx << std::endl;
        //std::cout << "dy: " << dy << std::endl;
        //std::cout << "ds: " << ds << std::endl;
    }

    return hessVect;


}

void FastHessian::getInterpolatePos(int x, int y, FilteredLayer* high, FilteredLayer* middle, FilteredLayer* base, double &xInter, double &yInter, double &sigInter) {

    Mat hessVect = getHessianVector(x, y, high, middle, base);
    Mat hessMat = getHessianMatrix(x, y, high, middle, base);

    Mat resultMat = Mat::zeros(3, 1, CV_64FC1);
    Mat invHessMat = hessMat.inv();

    gemm(invHessMat, hessVect, -1, 0, 0, resultMat, 0);

    /*for (int i = 0; i < hessMat.rows; ++i) {
        for (int j = 0; j < hessMat.cols; ++j)
            std::cout << " " << hessMat.at<double>(i, j);

        std::cout << std::endl;
    }

    for (int i = 0; i < hessVect.rows; ++i) {
        for (int j = 0; j < hessVect.cols; ++j)
            std::cout << " " << hessVect.at<double>(i, j);

        std::cout << std::endl;
    }
     */

    xInter = resultMat.at<double>(0, 0);
    yInter = resultMat.at<double>(1, 0);
    sigInter = resultMat.at<double>(2, 0);

}

void FastHessian::interpolateExtremum(int x, int y, FilteredLayer* high, FilteredLayer* middle, FilteredLayer* base) {

    int filterStep = (high->GetFilterSize() - middle->GetFilterSize());

    assert(filterStep > 0 && filterStep == middle->GetFilterSize() - base->GetFilterSize());

    double xInter = 0;
    double yInter = 0;
    double sigInter = 0;

    // Get the x, y variance in scale
    getInterpolatePos(x, y, high, middle, base, xInter, yInter, sigInter);

    //std::cout << " x: " << xInter << " - y: " << yInter << " - sig: " << sigInter << std::endl;

    if (fabs(xInter) < 0.5f && fabs(yInter) < 0.5f && fabs(sigInter) < 0.5f) {

        float xIPoint = static_cast<float> ((x + xInter) * high->GetStep());
        float yIPoint = static_cast<float> ((y + yInter) * high->GetStep());
        float scaleIPoint = static_cast<float> ((0.1333f)*(middle->GetFilterSize() + sigInter * filterStep));
        int scale = middle->GetWidth() / high->GetWidth();
        InterestPoint iPt(xIPoint, yIPoint, scaleIPoint, middle->getLaplacian(x, y, scale));

        //        std::cout << "called X: " << x << " Called y : " << y << " x: " << iPt.GetX() << " - y: " << iPt.GetY() << " - sig: " << iPt.GetScale() << std::endl;


        _iptVect.push_back(iPt);

    }
}