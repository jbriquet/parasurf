/*
 * File:   InterestPoint.cpp
 * Author: jules
 *
 * Created on 19 juin 2014, 15:19
 */

#include <math.h>
#include <float.h>

#include "InterestPoint.h"

InterestPoint::InterestPoint(float x, float y, float scale, int laplacian)
: _x(x), _y(y), _scale(scale), _orientation(0), _laplacian(laplacian) {
}

InterestPoint::InterestPoint() {
}

InterestPoint::~InterestPoint() {
}

void InterestPoint::SetLaplacian(int _laplacian) {
    this->_laplacian = _laplacian;
}

int InterestPoint::GetLaplacian() const {
    return _laplacian;
}

void InterestPoint::SetOrientation(float _orientation) {
    this->_orientation = _orientation;
}

float InterestPoint::GetOrientation() const {
    return _orientation;
}

void InterestPoint::SetScale(float _scale) {
    this->_scale = _scale;
}

float InterestPoint::GetScale() const {
    return _scale;
}

void InterestPoint::SetY(float _y) {
    this->_y = _y;
}

float InterestPoint::GetY() const {
    return _y;
}

void InterestPoint::SetX(float _x) {
    this->_x = _x;
}

float InterestPoint::GetX() const {
    return _x;
}

float* InterestPoint::GetDescriptor() {
    return _descriptor;
}

float operator-(InterestPoint &ip1, InterestPoint &ip2) {
    float sum = 0.f;

    for (int i = 0; i < 64; ++i)
        sum += (ip1.GetDescriptor()[i] - ip2.GetDescriptor()[i]) * (ip1.GetDescriptor()[i] - ip2.GetDescriptor()[i]);
    return sqrt(sum);
}

void matchIpoints(vector<InterestPoint> &ipts1, vector<InterestPoint> &ipts2, vector<pair<InterestPoint, InterestPoint> > &matches) {
    float d1, d2, distance;
    InterestPoint *matchIp;

    for (int i = 0; i < ipts1.size(); ++i) {
        d1 = d2 = FLT_MAX;
        for (int j = 0; j < ipts2.size(); ++j) {
            distance = ipts1[i] - ipts2[j];

            if (distance < d1) {
                d2 = d1;
                d1 = distance;
                matchIp = &ipts2[j];
            } else if (distance < d2)
                d2 = distance;
        }

        if (d1 / d2 < 0.65)
            matches.push_back(make_pair(ipts1[i], *matchIp));
    }
}

int translateCorners(vector<pair<InterestPoint, InterestPoint> > &matches, const Point src_corners[4], Point dst_corners[4]) {
    double h[9];
    Mat _h = Mat(3, 3, CV_64F, h);
    vector<Point2f> pt1, pt2;
    Mat _pt1, _pt2;

    int n = (int) matches.size();
    if (n < 4) return 0;

    // Set vectors to correct size
    pt1.resize(n);
    pt2.resize(n);

    // Copy Ipoints from match vector into cvPoint vectors
    for (int i = 0; i < n; i++) {
        pt1[i] = Point2f(matches[i].second.GetX(), matches[i].second.GetX());
        pt2[i] = Point2f(matches[i].first.GetX(), matches[i].first.GetY());
    }
    _pt1 = Mat(1, n, CV_32FC2, &pt1[0]);
    _pt2 = Mat(1, n, CV_32FC2, &pt2[0]);

    // Find the homography (transformation) between the two sets of points
    _h = findHomography(_pt1, _pt2, CV_RANSAC, 5);

    // Translate src_corners to dst_corners using homography
    for (int i = 0; i < 4; i++) {
        double x = src_corners[i].x, y = src_corners[i].y;
        double Z = 1. / (h[6] * x + h[7] * y + h[8]);
        double X = (h[0] * x + h[1] * y + h[2]) * Z;
        double Y = (h[3] * x + h[4] * y + h[5]) * Z;
        dst_corners[i] = Point(round(X), round(Y));
    }
    return 1;
}