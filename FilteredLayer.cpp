/*
 * File:   FilteredLayer.cpp
 * Author: jules
 *
 * Created on 19 juin 2014, 17:59
 */

#include <assert.h>
#include <string.h>
#include <math.h>

#include "FilteredLayer.h"

FilteredLayer::FilteredLayer(int width, int height, int step, int filterSize)
: _width(width), _height(height), _step(step), _filterSize(filterSize) {

    assert(width > 0 && height > 0);

    _hessiansDet = new float[width * height];
    memset(_hessiansDet, 0, sizeof (float) * width * height);

    _laplacian = new bool[width * height];
    memset(_laplacian, 0, sizeof (bool) * width * height);
}

float FilteredLayer::getScaledHessianDet(int x, int y, int scale) {
    return this->_hessiansDet[scale * y * this->_width + scale * x];
}

float FilteredLayer::getHessianDet(int x, int y) {
    return this->_hessiansDet[y * this->_width + x];
}

bool FilteredLayer::getLaplacian(int x, int y, int scale) {
    return _laplacian[(scale * y) * _width + (scale * x)];
}

FilteredLayer::~FilteredLayer() {
}

void FilteredLayer::SetFilterSize(int _filterSize) {
    this->_filterSize = _filterSize;
}

int FilteredLayer::GetFilterSize() const {
    return _filterSize;
}

void FilteredLayer::SetStep(int _step) {
    this->_step = _step;
}

int FilteredLayer::GetStep() const {
    return _step;
}

void FilteredLayer::SetHeight(int _height) {
    this->_height = _height;
}

int FilteredLayer::GetHeight() const {
    return _height;
}

void FilteredLayer::SetWidth(int _width) {
    this->_width = _width;
}

int FilteredLayer::GetWidth() const {
    return _width;
}

void FilteredLayer::SetHessiansDet(float* hessiansDet) {
    this->_hessiansDet = hessiansDet;
}

float* FilteredLayer::GetHessiansDet() const {
    return _hessiansDet;
}

void FilteredLayer::setLaplacian(bool* _laplacian) {
    this->_laplacian = _laplacian;
}

bool* FilteredLayer::getLaplacian() const {
    return _laplacian;
}

