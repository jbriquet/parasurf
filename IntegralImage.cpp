/*
 * File:   IntegralImage.cpp
 * Author: jules
 *
 * Created on 18 juin 2014, 16:33
 */

#include "IntegralImage.h"
#include <algorithm>

IntegralImage::IntegralImage(Mat* srcImg) {

    Mat grayCpy(*srcImg);
    cvtColor(grayCpy, grayCpy, CV_BGR2GRAY);
    Mat gray32F;
    grayCpy.convertTo(gray32F, CV_32FC1);
    this->_integralImg = Mat::zeros(srcImg->size(), CV_32FC1);

    float carry = 0.0f;

    for (int col = 0; col < _integralImg.cols; ++col) {
        carry += gray32F.at<float>(0, col) / (float) 255;
        _integralImg.at<float>(0, col) = carry;

        /*if (col == 1) {
            std::cout << "Init" << _integralImg.at<float>(0, col) << ": " << ret << std::endl;
        }*/
    }

    for (int row = 1; row < _integralImg.rows; ++row) {
        carry = 0.0f;
        for (int col = 0; col < _integralImg.cols; ++col) {
            carry += gray32F.at<float>(row, col) / 255;
            _integralImg.at<float>(row, col) = carry + _integralImg.at<float>(row - 1, col);
        }
    }
}

float IntegralImage::AreaIntegral(int xOrigin, int yOrigin, int width, int height) {

    // Avoid bad values in xOrigin, yOrigin, width and height
    int yOri = std::min(yOrigin, _integralImg.rows) - 1;
    int xOri = std::min(xOrigin, _integralImg.cols) - 1;

    int yFin = std::min(yOrigin + height, _integralImg.rows) - 1;
    int xFin = std::min(xOrigin + width, _integralImg.cols) - 1;


    float A = 0.0f;
    float B = 0.0f;
    float C = 0.0f;
    float D = 0.0f;

    // Verification for values < 0
    if (xOri >= 0 && yOri >= 0)
        A = _integralImg.at<float>(yOri, xOri);
    if (yOri >= 0 && xFin >= 0)
        B = _integralImg.at<float>(yOri, xFin);
    if (xOri >= 0 && yFin >= 0)
        C = _integralImg.at<float>(yFin, xOri);
    if (xFin >= 0 && yFin >= 0)
        D = _integralImg.at<float>(yFin, xFin);

    return std::max(0.0f, A - B - C + D);
}

float IntegralImage::Box(int col, int row, int rows, int cols) {

    // The subtraction by one for row/col is because row/col is inclusive.
    int r1 = std::min(row, _integralImg.rows) - 1;
    int c1 = std::min(col, _integralImg.cols) - 1;
    int r2 = std::min(row + rows, _integralImg.rows) - 1;
    int c2 = std::min(col + cols, _integralImg.cols) - 1;

    float A(0.0f), B(0.0f), C(0.0f), D(0.0f);
    if (r1 >= 0 && c1 >= 0) A = _integralImg.at<float>(r1, c1);
    if (r1 >= 0 && c2 >= 0) B = _integralImg.at<float>(r1, c2);
    if (r2 >= 0 && c1 >= 0) C = _integralImg.at<float>(r2, c1);
    if (r2 >= 0 && c2 >= 0) D = _integralImg.at<float>(r2, c2);

    return std::max(0.f, A - B - C + D);
}

void IntegralImage::SetIntegralImg(Mat _integralImg) {

    this->_integralImg = _integralImg;
}

Mat IntegralImage::GetIntegralImg() const {

    return _integralImg;
}

IntegralImage::~IntegralImage() {
}

