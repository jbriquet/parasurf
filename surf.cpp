#include "surf.h"

const float pi = 3.14159f;

const double gauss25 [7][7] = {
    0.02350693969273, 0.01849121369071, 0.01239503121241, 0.00708015417522, 0.00344628101733, 0.00142945847484, 0.00050524879060,
    0.02169964028389, 0.01706954162243, 0.01144205592615, 0.00653580605408, 0.00318131834134, 0.00131955648461, 0.00046640341759,
    0.01706954162243, 0.01342737701584, 0.00900063997939, 0.00514124713667, 0.00250251364222, 0.00103799989504, 0.00036688592278,
    0.01144205592615, 0.00900063997939, 0.00603330940534, 0.00344628101733, 0.00167748505986, 0.00069579213743, 0.00024593098864,
    0.00653580605408, 0.00514124713667, 0.00344628101733, 0.00196854695367, 0.00095819467066, 0.00039744277546, 0.00014047800980,
    0.00318131834134, 0.00250251364222, 0.00167748505986, 0.00095819467066, 0.00046640341759, 0.00019345616757, 0.00006837798818,
    0.00131955648461, 0.00103799989504, 0.00069579213743, 0.00039744277546, 0.00019345616757, 0.00008024231247, 0.00002836202103
};

Surf::Surf(IntegralImage* srcImg, vector<InterestPoint> &iPs)
: idx_(0),
iPs_(iPs) {
    img_ = srcImg;
}

Surf::~Surf() {
}

void Surf::descriptors(bool upRight) {
    if (iPs_.empty())
        return;

    for (int i = 0; i < iPs_.size(); ++i) {
        idx_ = i;

        if (!upRight)
            orientation();
        descriptor(upRight);
    }
}

vector<InterestPoint> &Surf::getInterestPoints() const {
    return iPs_;
}

void Surf::orientation() {
    InterestPoint* iPoint = &iPs_[idx_];
    float gauss = 0.f;
    int scale = round(iPoint->GetScale());
    int x = round(iPoint->GetX());
    int y = round(iPoint->GetY());
    const int mask[] = {6, 5, 4, 3, 2, 1, 0, 1, 2, 3, 4, 5, 6};
    vector<float> resX, resY, angs;

    float sumX, sumY, max, orientation, ang;


    for (int i = -6; i <= 6; ++i)
        for (int j = -6; j <= 6; ++j)
            if (i * i + j * j < 36) {
                gauss = static_cast<float> (gauss25[mask[i + 6]][j + 6]);
                resX.push_back(gauss * HaarX(x + i * scale, y + j * scale, 4
                        * scale));
                resY.push_back(gauss * HaarY(x + i * scale, y + j * scale, 4
                        * scale));
                angs.push_back(angle(resX.back(), resY.back()));
            }

    for (float atmp = 0.f; atmp < 2 * pi; atmp += 0.15f) {
        ang = (atmp + pi / 3.0f > 2 * pi ? atmp - 5.0f * pi / 3.0f : atmp + pi / 3.0f);
        sumX = sumY = 0.f;

        for (int i = 0; i < angs.size(); ++i) {
            const float angle = angs[i];

            if ((atmp < ang && atmp < angle && angle < ang) ||
                    (ang < atmp &&
                    ((angle > 0 && angle < ang) ||
                    (angle > atmp && angle < 2 * pi)))) {
                sumX += resX[i];
                sumY += resY[i];
            }
        }

        if (sumX * sumX + sumY * sumY > max) {
            max = sumX * sumX + sumY * sumY;
            orientation = angle(sumX, sumY);
        }
    }

    iPoint->SetOrientation(orientation);
}

float Surf::angle(float x, float y) {
    if (x > 0 && y >= 0)
        return atan(y / x);

    if (x < 0 && y >= 0)
        return pi - atan(-y / x);

    if (x < 0 && y < 0)
        return pi + atan(y / x);

    if (x > 0 && y < 0)
        return 2 * pi - atan(-y / x);

    return 0;
}

void Surf::descriptor(bool upRight) {
    InterestPoint *ipt = &iPs_[idx_];

    int sx, sy;
    int count = 0;
    int i = -8, ix = 0, j = 0, jx = 0, scx = 0, scy = 0;
    float dx, dy, mdx, mdy;
    float cosinus = 1.f, sinus = 0.f;
    float gauss1 = 0.f, gauss2 = 0.f;
    float rx = 0.f, ry = 0.f, rrx = 0.f, rry = 0.f, len = 0.f;
    float cx = -0.5f, cy;

    int x = round(ipt->GetX());
    int y = round(ipt->GetY());
    float scale = ipt->GetScale();
    float *d = ipt->GetDescriptor();
    
    if (!upRight) {
        cosinus = cos(ipt->GetOrientation());
        sinus = sin(ipt->GetOrientation());
    }

    while (i < 12) {
        j = -8;
        i -= 4;
        cx += 1.f;
        cy = -0.5f;

        while (j < 12) {
            dx = dy = mdx = mdy = 0.f;
            cy += 1.f;
            j -= 4;
            ix = i + 5;
            jx = j + 5;
            scx = round(x + (-jx * scale * sinus + ix * scale * cosinus));
            scy = round(y + (jx * scale * cosinus + ix * scale * sinus));

            for (int k = i; k < i + 9; ++k)
                for (int l = j; l < j + 9; ++l) {
                    sx = round(x + (-l * scale * sinus + k * scale * cosinus));
                    sy = round(y + (l * scale * cosinus + k * scale * sinus));

                    gauss1 = gaussian2d(scx - sx, scy - sy, 2.5f * scale);
                    rx = HaarX(sx, sy, 2 * round(scale));
                    ry = HaarY(sx, sy, 2 * round(scale));

                    rrx = gauss1 * (-rx * sinus + ry * cosinus);
                    rry = gauss1 * (rx * cosinus + ry * sinus);
                    
                    dx += rrx;
                    dy += rry;
                    mdx += fabs(rrx);
                    mdy += fabs(rry);
                }

            gauss2 = gaussian2d(cx - 2.0f, cy - 2.0f, 1.5f);

            d[count++] = dx * gauss2;
            d[count++] = dy * gauss2;
            d[count++] = mdx * gauss2;
            d[count++] = mdy * gauss2;

            len += (dx * dx + dy * dy + mdx * mdx + mdy * mdy) * gauss2 * gauss2;
            j += 9;
        }
        i += 9;
    }

    len = sqrt(len);
    for (int i = 0; i < 64; ++i)
        d[i] /= len;
}

float Surf::HaarX(int x, int y, int size) {
    return img_->AreaIntegral(x, y - size / 2, size / 2, size)
            - 1 * img_->AreaIntegral(x - size / 2, y - size / 2, size / 2, size);
}

float Surf::HaarY(int x, int y, int size) {
    return img_->AreaIntegral(x - size / 2, y, size, size / 2)
            - 1 * img_->AreaIntegral(x - size / 2, y - size / 2, size, size / 2);
}

inline float Surf::gaussian2d(float x, float y, float sigma) {
    return 1.0f / (2.0f * pi * sigma * sigma) * exp(-(x * x + y * y) / (2.0f * sigma * sigma));
}
