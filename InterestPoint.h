/*
 * File:   InterestPoint.h
 * Author: jules
 *
 * Created on 19 juin 2014, 15:19
 */

#ifndef INTERESTPOINT_H
#define	INTERESTPOINT_H

#include <vector>

#include "IntegralImage.h"

using namespace std;

class InterestPoint;

float operator-(const InterestPoint &ip1, const InterestPoint &ip2);
void matchIpoints(vector<InterestPoint> &ipts1, vector<InterestPoint> &ipts2, vector<pair<InterestPoint, InterestPoint> > &matches);
int translateCorners(vector<pair<InterestPoint, InterestPoint> > &matches, const Point src_corners[4], Point dst_corners[4]);

class InterestPoint {
public:
    InterestPoint(float x, float y, float scale, int laplacian);
    InterestPoint();
    virtual ~InterestPoint();
    void SetLaplacian(int _laplacian);
    int GetLaplacian() const;
    void SetOrientation(float _orientation);
    float GetOrientation() const;
    void SetScale(float _scale);
    float GetScale() const;
    float* GetDescriptor();
    void SetY(float _y);
    float GetY() const;
    void SetX(float _x);
    float GetX() const;

private:

    // Coordinates
    float _x;
    float _y;

    // Scale when the point is detected
    float _scale;

    // Anti-clockwise orientation
    float _orientation;

    // Sign of laplacian
    int _laplacian;

    // Descriptor
    float _descriptor[64];
};


#endif	/* INTERESTPOINT_H */

