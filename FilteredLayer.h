/*
 * File:   FilteredLayer.h
 * Author: jules
 *
 * Created on 19 juin 2014, 17:59
 */

#ifndef FILTEREDLAYER_H
#define	FILTEREDLAYER_H

class FilteredLayer {
public:
    FilteredLayer(int width, int height, int step, int filterSize);
    virtual ~FilteredLayer();

    float getScaledHessianDet(int x, int y, int scale);
    float getHessianDet(int x, int y);
    bool getLaplacian(int x, int y, int scale);


    void SetFilterSize(int _filterSize);
    int GetFilterSize() const;
    void SetStep(int _step);
    int GetStep() const;
    void SetHeight(int _height);
    int GetHeight() const;
    void SetWidth(int _width);
    int GetWidth() const;
    void SetHessiansDet(float* hessiansDet);
    float* GetHessiansDet() const;
    void setLaplacian(bool* _laplacian);
    bool* getLaplacian() const;


private:
    int _width;
    int _height;
    int _step;
    int _filterSize;

    float *_hessiansDet;
    bool *_laplacian;
};

#endif	/* FILTEREDLAYER_H */

