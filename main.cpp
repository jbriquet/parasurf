/*
 * File:   main.cpp
 * Author: jules
 *
 * Created on 13 juillet 2013, 15:44
 */

// Test application for the Visual Studio Image Watch Debugger extension

#include <iostream>                        // std::cout
#include <opencv2/core/core.hpp>           // cv::Mat
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>     // cv::imread()
#include <opencv2/imgproc/imgproc.hpp>     // cv::Canny()
#include <opencv2/video/video.hpp>
#include <boost/program_options.hpp>
#include <tbb/parallel_for.h>
#include <tbb/blocked_range.h>
#include <iomanip>

#include <ctime>

#include "tbb/task_scheduler_init.h"


#include "IntegralImage.h"
#include "FastHessian.h"
#include "InterestPoint.h"
#include "surf.h"

using namespace std;
using namespace cv;
using namespace boost::program_options;

typedef struct {
    Mat ImageRef;
    Mat ImageFromVid;

    vector<InterestPoint> InterestPointRef;
    vector<InterestPoint> InterestPointVid;

}
imagesData;

static imagesData globalDataImg = {
};

pthread_mutex_t mutex_for_some_value = PTHREAD_MUTEX_INITIALIZER;
bool isAlive = false;

void drawPoints(Mat *img, const vector<InterestPoint>& ipts) {
    float s, o;
    float r1, c1;

    std::cout << ipts.size() << " interest point found" << std::endl;

    for (unsigned int i = 0; i < ipts.size(); i++) {
        s = 3;

        InterestPoint ip = ipts[i];

        r1 = roundf(ip.GetY());
        c1 = roundf(ip.GetX());

        std::cout << "Point - x: " << c1 << " - y: " << r1 << std::endl;

        CvScalar color = Scalar(0, 0, 255);
        circle(*img, Point(c1, r1), s, color, 2);
    }
}

void drawIPoints(Mat *img, vector<InterestPoint>& ipts) {
    InterestPoint *ip;
    float s, o;
    int r1, c1, r2, c2, lap;
    int tailSize = 0;

    for (int i = 0; i < ipts.size(); ++i) {
        ip = &ipts.at(i);
        s = (2.5f * ip->GetScale());
        o = ip->GetOrientation();
        lap = ip->GetLaplacian();
        r1 = roundf(ip->GetY());
        c1 = roundf(ip->GetX());
        c2 = roundf(s * cos(o)) + c1;
        r2 = roundf(s * sin(o)) + r1;

        if (o) // Green line indicates orientation
            line(*img, Point(c1, r1), Point(c2, r2), cvScalar(0, 255, 0));
        else // Green dot if using upright version
            circle(*img, Point(c1, r1), 1, Scalar(0, 255, 0), -1);

        if (lap == 1) { // Blue circles indicate dark blobs on light backgrounds
            circle(*img, Point(c1, r1), roundf(s), Scalar(255, 0, 0), 1);
        } else if (lap == 0) { // Red circles indicate light blobs on dark backgrounds
            circle(*img, Point(c1, r1), roundf(s), Scalar(0, 0, 255), 1);
        } else if (lap == 9) { // Red circles indicate light blobs on dark backgrounds
            circle(*img, Point(c1, r1), roundf(s), Scalar(0, 255, 0), 1);
        }

        // Draw motion from ipoint dx and dy
        if (tailSize) {
            //      cvLine(img, cvPoint(c1,r1),
            //        cvPoint(int(c1+ip->dx*tailSize), int(r1+ip->dy*tailSize)),
            //        cvScalar(255,255,255), 1);
        }
    }
}

void drawRectangle(Mat *img, Mat &obj, vector<pair<InterestPoint, InterestPoint> > &matches) {
    Point obj_corners[4];
    Point scene_corners[4];

    obj_corners[0] = Point(0, 0);
    obj_corners[1] = Point(obj.cols, 0);
    obj_corners[2] = Point(obj.cols, obj.rows);
    obj_corners[3] = Point(0, obj.rows);

    translateCorners(matches, obj_corners, scene_corners);

    //    rectangle(*img, scene_corners[0], scene_corners[2], Scalar(0, 255, 0));
    line(*img, scene_corners[0] + Point(obj.cols, 0), scene_corners[1] + Point(obj.cols, 0), Scalar(0, 255, 0), 4);
    line(*img, scene_corners[1] + Point(obj.cols, 0), scene_corners[2] + Point(obj.cols, 0), Scalar(0, 255, 0), 4);
    line(*img, scene_corners[2] + Point(obj.cols, 0), scene_corners[3] + Point(obj.cols, 0), Scalar(0, 255, 0), 4);
    line(*img, scene_corners[3] + Point(obj.cols, 0), scene_corners[0] + Point(obj.cols, 0), Scalar(0, 255, 0), 4);
}

void *SurfProcessing(void *arg) {

    pthread_mutex_lock(&mutex_for_some_value);

    isAlive = true;

    // Copy Images Variables
    imagesData cpyDataImg = {
        .ImageRef = globalDataImg.ImageRef.clone(),
        .ImageFromVid = globalDataImg.ImageFromVid.clone(),

        .InterestPointRef = globalDataImg.InterestPointRef
    };

    pthread_mutex_unlock(&mutex_for_some_value);

    FastHessian fh2(&(cpyDataImg.ImageFromVid));

    Surf surf2(fh2.GetIntegralImg(), fh2.GetIptVect());
    surf2.descriptors(false);

    vector<pair<InterestPoint, InterestPoint> > matches;

    matchIpoints(cpyDataImg.InterestPointRef, surf2.getInterestPoints(), matches);

    std::cout << matches.size() << std::endl;


    cpyDataImg.InterestPointVid.clear();

    for (auto p : matches) {
        //globalDataImg.InterestPointRef.push_back(p.first);
        cpyDataImg.InterestPointVid.push_back(p.second);
    }

    pthread_mutex_lock(&mutex_for_some_value);
    // Assign the cpy to globalData
    globalDataImg = {
        .ImageRef = cpyDataImg.ImageRef.clone(),
        .ImageFromVid = cpyDataImg.ImageFromVid.clone(),

        .InterestPointRef = cpyDataImg.InterestPointRef,
        .InterestPointVid = cpyDataImg.InterestPointVid
    };

    isAlive = false;
    pthread_mutex_unlock(&mutex_for_some_value);

    pthread_exit(NULL);

}

int main(int argc, char *argv[]) {

    pthread_t my_thread;
    int ret;

    globalDataImg.ImageRef = imread(argv[1], CV_LOAD_IMAGE_COLOR);
    if (argc == 3)
        globalDataImg.ImageFromVid = imread(argv[2], CV_LOAD_IMAGE_COLOR);

    VideoCapture camVid(0);

    if (!(globalDataImg.ImageRef).data) {
        std::cout << "Could not open or find the image" << std::endl;
        return -1;
    }

    namedWindow("Display img2");
    namedWindow("Display img");
    imshow("Display img", globalDataImg.ImageRef);

    FastHessian fh(&(globalDataImg.ImageRef));
    Surf surf(fh.GetIntegralImg(), fh.GetIptVect());
    surf.descriptors(false);

    globalDataImg.InterestPointRef = surf.getInterestPoints();




    while (true) {
        if (argc != 3)
            camVid >> globalDataImg.ImageFromVid;

        if (!isAlive)
            ret = pthread_create(&my_thread, NULL, &SurfProcessing, NULL);

        //        drawIPoints(&image, match1);
        drawIPoints(&globalDataImg.ImageRef, globalDataImg.InterestPointRef);
        drawIPoints(&globalDataImg.ImageFromVid, globalDataImg.InterestPointVid);
        //        drawRectangle(&image2, image, matches);

        int c = cvWaitKey(1);
        if ((char) c == 27) break;
        imshow("Display img", globalDataImg.ImageRef);
        imshow("Display img2", globalDataImg.ImageFromVid);

    }

    pthread_exit(NULL);
}
