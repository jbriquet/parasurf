#ifndef SURF_H
#define	SURF_H

#include <opencv2/opencv.hpp>
#include <vector>
#include <cmath>

#include "IntegralImage.h"
#include "InterestPoint.h"

using namespace cv;
using namespace std;

class Surf {
public:
    Surf(IntegralImage* srcImg, vector<InterestPoint> &iPs);
    ~Surf();

    vector<InterestPoint> &getInterestPoints() const;

    void descriptors(bool upRight = false);

private:
    int idx_;
    vector<InterestPoint> &iPs_;
    IntegralImage* img_;


    void orientation();
    float angle(float x, float y);

    void descriptor(bool upRight = false);

    float HaarX(int row, int column, int size);
    float HaarY(int row, int column, int size);

    inline float gaussian2d(float x, float y, float sigma);
};
#endif	/* SURF_H */
