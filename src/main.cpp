/*
 * File:   main.cpp
 * Author: jules
 *
 * Created on 13 juillet 2013, 15:44
 */

// Test application for the Visual Studio Image Watch Debugger extension

#include <iostream>                        // std::cout
#include <opencv2/core/core.hpp>           // cv::Mat
#include <opencv2/highgui/highgui.hpp>     // cv::imread()
#include <opencv2/imgproc/imgproc.hpp>     // cv::Canny()
#include <opencv2/video/video.hpp>
#include <boost/program_options.hpp>
#include <tbb/parallel_for.h>
#include <tbb/blocked_range.h>
#include "tbb/task_scheduler_init.h"

using namespace std;
using namespace cv;
using namespace boost::program_options;

void detectContourImg() {
    IplImage* imgTrain = cvLoadImage("./learn/DetectingContours.jpg");

    VideoCapture camVid(0);
    Mat frame;

    Mat matImgTrain = Mat(imgTrain);

    cvNamedWindow("Shape Finder");
    cvShowImage("Shape Finder", imgTrain);

    GaussianBlur(matImgTrain, matImgTrain, Size(5, 5), 0);

    IplImage* imgGrayScale = cvCreateImage(cvGetSize(imgTrain), 8, 1);
    Mat matImgGray = Mat(imgGrayScale);

    cvtColor(matImgTrain, matImgGray, CV_RGB2GRAY);

    threshold(matImgGray, matImgGray, 100, 255, CV_THRESH_BINARY_INV);

    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;

    findContours(matImgGray, contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, cvPoint(0, 0));

    vector< vector<Point> > contours_poly(contours.size());

    Mat drawing = Mat(matImgGray.size(), CV_8UC3);
    Scalar color;
    for (int i = 0; i < contours.size(); i++) {

        approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
        color = Scalar(255, 255, 0);

        // Three
        if (contours_poly[i].size() == 3 && fabs(contourArea(contours_poly[i], true)) > 100) {
            //iterating through each point
            std::cout << "Triangle" << std::endl;
            color = Scalar(255, 0, 0);

        }//if there are 4 vertices in the contour(It should be a quadrilateral)
        if (contours_poly[i].size() == 4) {
            color = Scalar(0, 255, 0);
            std::cout << "Quadri" << std::endl;

        }//if there are 7  vertices  in the contour(It should be a heptagon)
        else if (contours_poly[i].size() == 7) {



            std::cout << "hepta" << std::endl;
            color = Scalar(0, 0, 255);
        }

        drawContours(drawing, contours_poly, i, color, 3, 8, vector<Vec4i>(), 0, Point());

    }

    cvNamedWindow("Result");
    imshow("Result", drawing);

    cvWaitKey(0);

    cvDestroyAllWindows();
    cvReleaseImage(&imgTrain);
    cvReleaseImage(&imgGrayScale);
}

Mat trackObject(const Mat& imgTresh) {

    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;

    findContours(imgTresh, contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, cvPoint(0, 0));

    vector< vector<Point> > contours_poly(contours.size());

    Mat drawing = Mat(imgTresh.size(), CV_8UC3);
    Scalar color;

    for (int i = 0; i < contours.size(); i++) {

        approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
        color = Scalar(255, 255, 0);

        // Three
        if (contours_poly[i].size() == 3 && fabs(contourArea(contours_poly[i], true)) > 100) {
            //iterating through each point
            std::cout << "Triangle" << std::endl;
            color = Scalar(255, 0, 0);

        }//if there are 4 vertices in the contour(It should be a quadrilateral)
        if (contours_poly[i].size() == 4) {
            color = Scalar(0, 255, 0);
            std::cout << "Quadri" << std::endl;

        }//if there are 7  vertices  in the contour(It should be a heptagon)
        else if (contours_poly[i].size() == 7) {



            std::cout << "hepta" << std::endl;
            color = Scalar(0, 0, 255);
        }

        drawContours(drawing, contours_poly, i, color, 3, 8, vector<Vec4i>(), 0, Point());

    }

    return drawing;

}

int testShape() {
    cvNamedWindow("ShapeFinder");
    cvNamedWindow("Normal");

    VideoCapture camVid(0);
    Mat frame;

    camVid >> frame;

    Mat imgTracking = Mat::zeros(frame.size(), CV_8UC3);
    Mat frameModif;
    Mat convertFrame;
    Mat finalFrame;

    for (;;) {

        // Get the new frame
        camVid >> frame;

        // Apply gaussian blur filter
        GaussianBlur(frame, frameModif, Size(5, 5), 0);

        // Apply grey scale filter
        cvtColor(frame, frameModif, CV_BGR2GRAY);

        // Threshold
        threshold(frameModif, frameModif, 150, 255, CV_THRESH_BINARY_INV);

        Canny(frameModif, frameModif, 100, 200, 3);

        frame.convertTo(convertFrame, CV_8UC3);

        //convertFrame = trackObject(frameModif);
        //convertFrame += frame;

        imshow("ShapeFinder", frameModif);
        imshow("Normal", frame);

        frame.release();
        frameModif.release();
        //convertFrame.release();

        int c = cvWaitKey(1);

        if ((char) c == 27) break;

    }

    cvDestroyAllWindows();

    return 0;
}

int testColor() {

    VideoCapture camVid(0);

    namedWindow("Control");

    int iLowH = 0;
    int iHighH = 179;

    int iLowS = 0;
    int iHighS = 255;

    int iLowV = 0;
    int iHighV = 255;

    cvCreateTrackbar("LowH", "Control", &iLowH, 179);
    cvCreateTrackbar("HighH", "Control", &iHighH, 179);

    cvCreateTrackbar("LowS", "Control", &iLowS, 255); //Saturation (0 - 255)
    cvCreateTrackbar("HighS", "Control", &iHighS, 255);

    cvCreateTrackbar("LowV", "Control", &iLowV, 255); //Value (0 - 255)
    cvCreateTrackbar("HighV", "Control", &iHighV, 255);

    Mat imgOrigin;
    Mat imgHSV;
    Mat imgThresholded;

    while (true) {

        camVid >> imgOrigin;

        cvtColor(imgOrigin, imgHSV, COLOR_BGR2HSV);
        inRange(imgHSV, Scalar(iLowH, iLowS, iLowV), Scalar(iHighH, iHighS, iHighV), imgThresholded);

        //morphological opening (remove small objects from the foreground)
        erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
        dilate(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));

        //morphological closing (fill small holes in the foreground)
        dilate(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
        erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));

        imshow("Thresholded Image", imgThresholded); //show the thresholded image
        imshow("Original", imgOrigin); //show the original image


        int c = cvWaitKey(1);

        if ((char) c == 27) break;

    }

    return 0;

}

int detectRedArrow() {
    VideoCapture camVid(0);

    Scalar redLow(0, 125, 100);
    Scalar redHigh(10, 255, 255);

    Mat originImg;
    Mat imgHSV;
    Mat imgShape;

    namedWindow("RedArrow Color");
    namedWindow("RedArrow Shape");

    while (true) {

        camVid >> originImg;

        // GET THE RED COLOR PART:
        cvtColor(originImg, imgHSV, COLOR_BGR2HSV);
        inRange(imgHSV, redLow, redHigh, imgHSV);
        //morphological opening (remove small objects from the foreground)
        erode(imgHSV, imgHSV, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
        dilate(imgHSV, imgHSV, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
        //morphological closing (fill small holes in the foreground)
        dilate(imgHSV, imgHSV, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
        erode(imgHSV, imgHSV, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));



        //cvtColor(originImg, imgShape, COLOR_HSV2BGR);
        // GET SHAPE PART
        // Apply gaussian blur filter
        GaussianBlur(originImg, imgShape, Size(5, 5), 0);

        // Apply grey scale filter
        cvtColor(imgShape, imgShape, CV_BGR2GRAY);

        // Threshold
        threshold(imgShape, imgShape, 150, 255, CV_THRESH_BINARY_INV);

        Canny(imgShape, imgShape, 100, 200, 3);

        imshow("RedArrow Color", imgHSV);
        imshow("RedArrow Shape", imgShape);

        // Exit when press echap
        int c = cvWaitKey(1);
        if ((char) c == 27) break;
    }


}

int main(int argc, char *argv[]) {
    detectRedArrow();
}

