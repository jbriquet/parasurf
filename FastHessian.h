/*
 * File:   FastHessian.h
 * Author: jules
 *
 * Created on 19 juin 2014, 15:27
 */

#ifndef FASTHESSIAN_H
#define	FASTHESSIAN_H

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "IntegralImage.h"
#include "FilteredLayer.h"
#include "InterestPoint.h"

using namespace cv;

static const int base_octave = 5;
static const int base_interval = 4;
static const int base_step = 2;
static const float base_threshold = 0.0004f;

class FastHessian {
public:
    FastHessian(Mat *img, const int octaves = base_octave, const int intervals = base_interval, const int step = base_step, const float threshold = base_threshold);
    virtual ~FastHessian();

    void ChangeParameters(const int octaves, const int intervals, const int step, const float threshold);
    void ComputeSpaceScale();
    void ComputeHessian(FilteredLayer* layer);
    bool isExtremum(int x, int y, FilteredLayer *high, FilteredLayer *middle, FilteredLayer *base);
    void interpolateExtremum(int x, int y, FilteredLayer* high, FilteredLayer* middle, FilteredLayer* base);

    Mat getHessianMatrix(int x, int y, FilteredLayer *high, FilteredLayer *middle, FilteredLayer *base);
    Mat getHessianVector(int x, int y, FilteredLayer *high, FilteredLayer *middle, FilteredLayer *base);
    void getInterpolatePos(int x, int y, FilteredLayer* high, FilteredLayer* middle, FilteredLayer* base, double &xInter, double &yInter, double &sigInter);


    void test(FilteredLayer* layer);
    void SetIptVect(std::vector<InterestPoint> _iptVect);
    std::vector<InterestPoint> &GetIptVect();
    IntegralImage* GetIntegralImg();

private:

    IntegralImage _integralImg;
    int _octavesNbr;
    int _intervalsNbr;
    int _step;
    float _thresholdValue;

    std::vector<FilteredLayer*> _filteredImgMap;
    std::vector<InterestPoint> _iptVect;
};

#endif	/* FASTHESSIAN_H */

