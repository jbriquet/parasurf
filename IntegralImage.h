/*
 * File:   IntegralImage.h
 * Author: jules
 *
 * Created on 18 juin 2014, 16:33
 */

#ifndef INTEGRALIMAGE_H
#define	INTEGRALIMAGE_H

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>

using namespace cv;

class IntegralImage {
public:
    IntegralImage(Mat* srcImg);
    virtual ~IntegralImage();

    float AreaIntegral(int xOrigin, int yOrigin, int width, int height);
    float Box(int row, int col, int rows, int cols);
    void SetIntegralImg(Mat _integralImg);
    Mat GetIntegralImg() const;
private:
    Mat _integralImg;

};

#endif	/* INTEGRALIMAGE_H */

